# portfolio

Faire votre portfolio pour le site simplonlyon.fr dans lequel devront figurés :
* Une petite présentation
* La liste de vos projets
* Un contact
* Le lien vers votre gitlab
* Eventuellement vos préférences en terme de langages/techno

Le site devra être responsive et valide W3C.

## Mise en ligne

Pour mettre le site en ligne, il faudra vous connecter en ssh au serveur simplonlyon.fr
puis aller dans votre dossier www et y faire un git clone de votre projet.

1. Se connecter en ssh au serveur (`ssh username@simplonlyon.fr`)
2. Aller dans le dossier www
3. Récupérer le contenu de votre conf.json et créer un conf.json dans votre projet à la racine (au même niveau que le index.html) et mettre le contenu copier dedans, mettre également votre avatar dans le projet (puis commit/push le tout)
4. Vider votre dossier www sur le serveur (avec des rm)
5. Cloner votre projet en https dans le dossier courant avec `git clone https://gitlab.com/username/portfolio .`